//
//  DataViewController.swift
//  DelegatesAssignement
//
//  Created by Rohit Marwaha on 12/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

protocol UpdateData{
    func update(text: String)
}


class DataViewController: UIViewController {
    var editText: String = ""
    var button: UIButton!
    var textField: UITextField!
 
    var updateData: UpdateData!
    
 
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        textField.frame = CGRect(x: 20.0, y: 100.0, width: self.view.frame.size.width - 40,
                                 height: 50)
        button.center = self.view.center
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        
        textField = UITextField()
        textField.borderStyle = .roundedRect
        textField.placeholder = "Enter your name"
        textField.text = editText
        self.view.addSubview(textField)
        
        button = UIButton(type: .system)
        button.setTitle("Done", for: UIControl.State.normal)
        button.frame = CGRect(x: 0.0, y: 100.0, width: 200, height: 40)
        self.view.addSubview(button)
        button.addTarget(self, action: #selector(backButtonClicked), for: .touchUpInside)
        
    }
    
    
    @objc func backButtonClicked() {
        
        let text =  textField.text ?? ""
        
        updateData.update(text: text)
        
        self.dismiss(animated: true) {
        }
        
    }


}
