//
//  SignUpViewController.swift
//  DelegatesAssignement
//
//  Created by Rohit Marwaha on 12/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    var backButton: UIButton!
    var button: UIButton!
    private var label: UILabel!
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        button.center = self.view.center
        label.frame = CGRect(x: 20.0, y: self.view.safeAreaInsets.top + 60 , width: self.view.frame.size.width - 40, height: 150)
        
        backButton.frame = CGRect(x: 0.0, y: self.view.safeAreaInsets.top, width: 200, height: 40)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view.backgroundColor = .white
        
        label = UILabel()
        label.textColor = UIColor.black
        label.backgroundColor = UIColor.lightGray
        self.view.addSubview(label)
        
        button = UIButton(type: .system)
        button.setTitle("Open Data View", for: UIControl.State.normal)
        self.view.addSubview(button)
        button.frame = CGRect(x: 0.0, y: 0.0, width: 200, height: 40)
        button.addTarget(self, action: #selector(nextButtonClicked), for: .touchUpInside)
        
        backButton = UIButton(type: .system)
        backButton.setTitle("Back", for: UIControl.State.normal)
        self.view.addSubview(backButton)
        backButton.contentMode = .left
        backButton.frame = CGRect(x: 0.0, y: 0.0, width: 200, height: 40)
        backButton.addTarget(self, action: #selector(backButtonClicked), for: .touchUpInside)
        
    }
    
    @objc func nextButtonClicked() {
        let dataViewController = DataViewController()
        dataViewController.updateData = self
        dataViewController.editText = label.text ?? ""
        self.present(dataViewController, animated: true, completion: nil)
    }
    
    @objc func backButtonClicked() {
        self.dismiss(animated: true) {
        }
    }


}

extension SignUpViewController: UpdateData{
    func update(text: String) {
        label.text = text
    }
    
    
}
