//
//  ViewController.swift
//  DelegatesAssignement
//
//  Created by Rohit Marwaha on 12/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func signUp(_ sender: Any) {
        let signupViewController = SignUpViewController()
        self.present(signupViewController, animated: true, completion: nil)

    }
    
    @IBAction func login(_ sender: Any) {
        let loginViewController = LoginViewController()
        self.present(loginViewController, animated: true, completion: nil)
    }
}

